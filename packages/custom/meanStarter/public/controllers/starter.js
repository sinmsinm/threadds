;(function () {
  'use strict'

  angular.module('mean.meanStarter')
    .controller('StarterController', StarterController)

  StarterController.$inject = ['$scope', 'Global','$location','$window']

  function StarterController ($scope, Global, $location,$window) {
    // Original scaffolded code.
    $scope.global = Global
    $scope.package = {
      name: 'meanStarter'
    }

    $scope.go = function (path) {
      console.log ('hello' + path);
      //$location.path (path);
      //We need to refresh full location in order to login.
      $window.location.href = path;
    };
  }


})()
