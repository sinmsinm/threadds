'use strict';

//Setting up route
angular.module('mean.threads').config(['$stateProvider',
  function($stateProvider) {

    // states for my app
    $stateProvider
      .state('all recent threads', {
        url: '/threads',
        templateUrl: '/threads/views/list.html',
        requiredCircles : {
          circles: ['authenticated'],
          denyState: 'auth.login'
        }
      })
      .state('all my threads', {
        url: '/threads/my',
        templateUrl: '/threads/views/list.html',
        requiredCircles : {
          circles: ['authenticated'],
          denyState: 'auth.login'
        }
      })
      .state('all popular threads', {
        url: '/threads/popular',
        templateUrl: '/threads/views/list.html',
        requiredCircles : {
          circles: ['authenticated'],
          denyState: 'auth.login'
        }
      })
      .state('create thread', {
        url: '/threads/create',
        templateUrl: '/threads/views/create.html',
        requiredCircles : {
          circles: ['can create content']
        }
      })
      .state('edit thread', {
        url: '/threads/:threadId/edit',
        templateUrl: '/threads/views/edit.html',
        requiredCircles : {
          circles: ['can edit content']
        }
      })
      .state('answer thread', {
        url: '/threads/:threadId/answer',
        templateUrl: '/threads/views/create.html',
        requiredCircles : {
          circles: ['can create content']
        }
      })
      .state('thread by id', {
        url: '/threads/:threadId',
        templateUrl: '/threads/views/view.html',
        requiredCircles : {
          circles: ['authenticated'],
          denyState: 'auth.login'
        }
      });
  }
]);
