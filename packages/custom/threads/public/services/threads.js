'use strict';

//Threads service used for threads REST endpoint
angular.module('mean.threads').factory('Threads', ['$resource',
  function($resource) {
    return $resource('api/threads/:parentId/:threadId/:cmd', {
      threadId: '@_id',
      parentId: '@parentId'
    }, {

      update: {
        method: 'PUT'
      },
      saveChild: {
        method: 'POST'
      },
      unpublish: {
        method: 'POST',
        params: {cmd: 'unpublish'}
      },
      publish: {
        method: 'POST',
        params: {cmd: 'publish'}
      },
      like: {
        method: 'POST',
        params: {cmd: 'like'}
      },
      unlike: {
        method: 'POST',
        params: {cmd: 'unlike'}
      },
      fetchFullThread: {
        method: 'GET',
        isArray: false,

        /*transformResponse: function(threads) {
          console.log ('accedint');
          threads = angular.fromJson(threads);
          threads.map(function(threads) {
            threads.likeIds = [];
            if (threads.likes) {
              for (var key in thread.likes) {
                console.log ("trobant ")
                var obj = thread.likes[key];
                thread.likeIds.push(obj._id);
              }
            }
          });
          return threads;
        }*/
      }
    });
  }
]).filter('newlines', function($sce) {
  return function(text) {
    if (text !== undefined) {
      return $sce.trustAsHtml(text.valueOf().replace(/\n/g, '<br/>'));
    }
  };
});
