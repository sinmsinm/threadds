'use strict';

angular.module('mean.threads').controller('ThreadsController', ['$scope', '$stateParams', '$location', 'Global', 'Threads', 'MeanUser', 'Circles', '$window','lodash',
  function($scope, $stateParams, $location, Global, Threads, MeanUser, Circles, $window, _) {
    $scope.global = Global;
    $scope.rows = 10;
    $scope.newThread = {};


    $scope.hasAuthorization = function(thread) {
      if (!thread || !thread.user) return false;
      return MeanUser.isAdmin || thread.user._id === MeanUser.user._id;
    };

    $scope.currentUserId = MeanUser.user._id;

    $scope.availableCircles = [];

    Circles.mine(function(acl) {
        $scope.availableCircles = acl.allowed;
        $scope.allDescendants = acl.descendants;
    });

    $scope.showDescendants = function(permission) {
        var temp = $('.ui-select-container .btn-primary').text().split(' ');
        temp.shift(); //remove close icon
        var selected = temp.join(' ');
        $scope.descendants = $scope.allDescendants[selected];
    };

    $scope.selectPermission = function() {
        $scope.descendants = [];
    };

    $scope.goCreateThread = function () {
      console.log ('hello');
      $location.path ("/threads/create");
    }

    $scope.back = function () {
      $window.history.back();
    }

    $scope.create = function(childnode) {
      var thread;
      if (!childnode) {
        // $scope.thread.permissions.push('test test');
         thread = new Threads($scope.newThread);
         thread.$save(function(response) {
           $location.path('/threads/' + response._id);
         });
      } else {
         thread = new Threads (
          {
          parentId: childnode._id,
          parent: childnode._id,
          root: childnode.root,
          title: childnode.title,
          body: childnode.response || $scope.newThread.body
        });

        console.log (thread);

        thread.$saveChild(function(response) {
          $scope.initAnswer();
          console.log ('the repsonse' + response.root);
          $location.path('/threads/' + response.root);
        });
      }

      $scope.newThread = {};
    };

    $scope.edit = function (thread) {
      $location.path ('/threads/'+ thread._id + '/edit');
    }

    $scope.remove = function(thread) {
      if (thread) {
        thread.$remove(function(response) {
          for (var i in $scope.threads) {
            if ($scope.threads[i] === thread) {
              $scope.threads.splice(i, 1);
            }
          }
          $location.path('threads');
        });
      } else {
        $scope.thread.$remove(function(response) {
          $location.path('threads');
        });
      }
    };

    $scope.unpublishComment = function (childnode) {
        var thread = new Threads (childnode);

        thread.$unpublish (function (response) {
          $scope.initAnswer();
        });
    };

    $scope.republishComment = function (childnode) {
        var thread = new Threads (childnode);

        thread.$publish (function (response) {
          $scope.initAnswer();
        });
    };

    $scope.like = function (childnode) {
      var thread = new Threads (childnode);

      thread.$like (function (response) {
        childnode.likes = response.likes;
        childnode.youLike = response.youLike;
      });
    }

    $scope.unlike = function (childnode) {
      var thread = new Threads (childnode);

      thread.$unlike (function (response) {
        childnode.likes = response.likes;
        childnode.youLike = response.youLike;
      });
    };

    $scope.update = function(isValid) {
      if (isValid) {
        var thread = $scope.thread;
        if (!thread.updated) {
          thread.updated = [];
        }
        thread.updated.push(new Date().getTime());

        thread.$update(function() {
          $location.path('threads/' + thread._id);
        });
      } else {
        $scope.submitted = true;
      }
    };

    $scope.clean = function () {
      $scope.newThread = {};
    };

    $scope.expandResponse = function ($event) {
      $scope.rows=200;
    };

    $scope.expandResponseComment = function (childnode) {
      childnode.reply = true;
      childnode.rows = 200;
      childnode.response = "";
      setTimeout (function () {
        var elem = $window.document.getElementById('response_' + childnode._id);
        elem.focus();
      }, 100);
    };

    $scope.collapseResponse = function () {
      if (!$scope.newThread.body || $scope.newThread.body.length == 0) {
        $scope.rows=200;
      }
    };

    $scope.collapseChildResponse = function (childnode){
      if (!childnode.response || childnode.response.length == 0) {
        childnode.reply = true;
      }
    };



    $scope.find = function() {

      var search = {};

      if ($location.path() === '/threads/my') {
        search.owner = MeanUser.user._id;
      }

      if ($location.path() === '/threads/popular') {
        search.threadId = 'popular';
      }

      Threads.query(search, function(threads) {
         $scope.threads = threads;
      });


    };

    $scope.findOne = function() {
      Threads.get({
        threadId: $stateParams.threadId
      }, function(thread) {
        $scope.thread = thread;
      });
    };

    $scope.initAnswer = function() {
      Threads.fetchFullThread ({
        threadId: $stateParams.threadId
      }, function(thread) {
        $scope.thread = thread;
        console.log ('eo',thread);
        $scope.newThread = {
          parent: thread._id,
          root: ($scope.thread.root ? $scope.thread.root : $scope.thread._id),
          title: $scope.thread.title
        };
        console.log ('newThread', $scope.thread);
      $scope.currentUsername = MeanUser.user.name;
      });
    };

  }
]).config(['$provide', function ($provide) {
        $provide.decorator('taOptions', ['$delegate', function (taOptions) {

            taOptions.forceTextAngularSanitize = true;
            taOptions.keyMappings = [];
            taOptions.toolbar = [
                ['h1', 'h2', 'h3', 'p', 'pre', 'quote'],
                ['bold', 'italics', 'underline', 'ul', 'ol', 'redo', 'undo', 'clear'],
                ['justifyLeft', 'justifyCenter', 'justifyRight'],
                ['html', 'insertImage', 'insertLink']
            ];
            taOptions.classes = {
                focussed: '',
                toolbar: 'ta-toolbar',
                toolbarGroup: 'ta-button-group',
                toolbarButton: '',
                toolbarButtonActive: 'active',
                disabled: 'disabled',
                textEditor: 'ta-text-editor',
                htmlEditor: 'md-input'
            };
            return taOptions; // whatever you return will be the taOptions
        }]);
        $provide.decorator('taTools', ['$delegate', function (taTools) {
            taTools.h1.display = '<md-button aria-label="Heading 1">H1</md-button>';
            taTools.h2.display = '<md-button aria-label="Heading 2">H2</md-button>';
            taTools.h3.display = '<md-button aria-label="Heading 3">H3</md-button>';
            taTools.p.display = '<md-button aria-label="Paragraph">P</md-button>';
            taTools.pre.display = '<md-button aria-label="Pre">pre</md-button>';
            taTools.quote.display = '<md-button class="md-icon-button" aria-label="Quote"><md-icon md-font-set="material-icons">format_quote</md-icon></md-button>';
            taTools.bold.display = '<md-button class="md-icon-button" aria-label="Bold"><md-icon md-font-set="material-icons">format_bold</md-icon></md-button>';
            taTools.italics.display = '<md-button class="md-icon-button" aria-label="Italic"><md-icon md-font-set="material-icons">format_italic</md-icon></md-button>';
            taTools.underline.display = '<md-button class="md-icon-button" aria-label="Underline"><md-icon md-font-set="material-icons">format_underlined</md-icon></md-button>';
            taTools.ul.display = '<md-button class="md-icon-button" aria-label="Buletted list"><md-icon md-font-set="material-icons">format_list_bulleted</md-icon></md-button>';
            taTools.ol.display = '<md-button class="md-icon-button" aria-label="Numbered list"><md-icon md-font-set="material-icons">format_list_numbered</md-icon></md-button>';
            taTools.undo.display = '<md-button class="md-icon-button" aria-label="Undo"><md-icon md-font-set="material-icons">undo</md-icon></md-button>';
            taTools.redo.display = '<md-button class="md-icon-button" aria-label="Redo"><md-icon md-font-set="material-icons">redo</md-icon></md-button>';
            taTools.justifyLeft.display = '<md-button class="md-icon-button" aria-label="Align left"><md-icon md-font-set="material-icons">format_align_left</md-icon></md-button>';
            taTools.justifyRight.display = '<md-button class="md-icon-button" aria-label="Align right"><md-icon md-font-set="material-icons">format_align_right</md-icon></md-button>';
            taTools.justifyCenter.display = '<md-button class="md-icon-button" aria-label="Align center"><md-icon md-font-set="material-icons">format_align_center</md-icon></md-button>';
            taTools.clear.display = '<md-button class="md-icon-button" aria-label="Clear formatting"><md-icon md-font-set="material-icons">format_clear</md-icon></md-button>';
            taTools.html.display = '<md-button class="md-icon-button" aria-label="Show HTML"><md-icon md-font-set="material-icons">code</md-icon></md-button>';
            taTools.insertLink.display = '<md-button class="md-icon-button" aria-label="Insert link"><md-icon md-font-set="material-icons">insert_link</md-icon></md-button>';
            taTools.insertImage.display = '<md-button class="md-icon-button" aria-label="Insert photo"><md-icon md-font-set="material-icons">insert_photo</md-icon></md-button>';
            return taTools;
        }]);
    }]);
