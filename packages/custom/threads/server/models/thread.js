'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Thread Schema
 */
var ThreadSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  changed: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    trim: true,
    required: true
  },
  body: {
    type: String,
    trim: true,
    required: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    index: true
  },
  mentionsUsers: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
  userPicture: {
    type: String
  },
  userName: {
    type: String
  },
  published : {
    type: Boolean,
    default: true
  },
  parent: {
    type: Schema.ObjectId,
    ref: 'Thread',
    index: true
  },
  parents: [{
    type: Schema.ObjectId,
    ref: 'Thread',
    index: true
  }],
  root: {
    type: Schema.ObjectId,
    ref: 'Thread',
    index: true
  },
  likes: [{
    type: Schema.ObjectId,
    ref: 'User'
  }],
  youLike : {
    type: Boolean,
    default: false
  }
});

/**
 * Validations
 */
ThreadSchema.path('title').validate(function(title) {
  return !!title;
}, 'Title cannot be blank');

ThreadSchema.path('body').validate(function(body) {
  return !!body;
}, 'Content cannot be blank');

/**
 * Statics
 */
ThreadSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).populate('user', 'name username').exec(cb);
};


ThreadSchema.set('versionKey', false);
mongoose.model('Thread', ThreadSchema);
