'use strict';

// Thread authorization helpers
var hasAuthorization = function(req, res, next) {
  if (!req.user.isAdmin && !req.thread.user._id.equals(req.user._id)) {
    return res.status(401).send('User is not authorized');
  }
  next();
};

var hasPermissions = function(req, res, next) {

    req.body.permissions = req.body.permissions || ['authenticated'];

    for (var i = 0; i < req.body.permissions.length; i++) {
      var permission = req.body.permissions[i];
      if (req.acl.user.allowed.indexOf(permission) === -1) {
            return res.status(401).send('User not allowed to assign ' + permission + ' permission.');
        }
    }

    next();
};


module.exports = function(Threads, app, auth, database, circles) {

  var requiresLogin = circles.controller.hasCircle('authenticated');

  var threads = require('../controllers/threads')(Threads);

  app.route('/api/threads')
    .get(threads.allRootThreads)
    .post(requiresLogin, hasPermissions, threads.create);

    app.route('/api/threads/popular')
      .get(threads.allRootThreadsByPopular);

  app.route('/api/threads/:threadId')
    .get(auth.isMongoId, threads.fetchByParent)
    .post(requiresLogin, hasPermissions, threads.create)
    .put(auth.isMongoId, requiresLogin, hasAuthorization, hasPermissions, threads.update)
    .delete(auth.isMongoId, requiresLogin, hasAuthorization, hasPermissions, threads.destroy);


app.route('/api/threads/:threadId/unpublish')
    .post(auth.isMongoId, hasAuthorization, threads.unpublish);

app.route('/api/threads/:threadId/publish')
    .post(auth.isMongoId, hasAuthorization, threads.publish);

app.route('/api/threads/:threadId/like')
    .post(auth.isMongoId, hasAuthorization, threads.like);

app.route('/api/threads/:threadId/unlike')
    .post(auth.isMongoId, hasAuthorization, threads.unlike);

  // Finish with setting up the threadId param
  app.param('threadId', threads.thread);
};
