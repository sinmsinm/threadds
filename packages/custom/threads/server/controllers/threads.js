'use strict'

/**
* Module dependencies.
*/
var mongoose = require('mongoose'),
Thread = mongoose.model('Thread'),
config = require('meanio').loadConfig();
var _ = require('lodash');
var partial = require('lodash.partial');
global._ = _;
require('tree-plugin-underscore');

module.exports = function(Threads) {

  var youLike = function (likes,user) {

    return _.some (likes, function (like){
      return (like._id.toString() === user._id.toString()) ;
    });

  };

  return {
    /**
    * Find thread element by id
    */
    thread: function(req, res, next, id) {
      Thread.load(id, function(err, thread) {
        if (err) return next(err);
        if (!thread) return next(new Error('Failed to load thread ' + id));
        req.thread = thread;
        next();
      });
    },
    /**
    * Create a thread element
    */
    create: function(req, res) {
      var thread = new Thread(req.body);
      var parentThread = req.thread;
      thread.user = req.user;

      console.log ('the parent', req.thread);

      // Lets link with the parent if it exists
      if (parentThread) {
        thread.parent = parentThread;
        thread.parents =  parentThread.parents || [];
        thread.parents.push (thread.parent);
        thread.root = parentThread.root || parentThread._id;
        console.log ('parentThread',parentThread);

      }


      thread.likes = [];
      thread.youLike = false;

      thread.save(function(err) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot save the thread'
          });
        } else {
          thread.populate({
            path: 'mentionsUsers',
            select: 'name username'
          }, function(err, doc) {
            console.log ('doc',doc);
            res.json(doc);
          });
        }

        Threads.events.publish({
          action: 'created',
          user: {
            name: req.user.name
          },
          url: config.hostname + '/threads/' + thread._id,
          name: thread.title
        });
      });
    },

    /**
    * Update a thread element
    */
    update: function(req, res) {
      var thread = req.thread;

      var mentions = [];
      var tagged_Users = [];
      if (req.body.mentionsUsers.length > 0) {

        _.map(req.body.mentionsUsers, function(mu) {
          // push client id (converted from string to mongo object id) into clients
          if (mu._id !== undefined) {
            tagged_Users.push(mu);
            mentions.push(mongoose.Types.ObjectId(mu._id));
          } else
          mentions.push(mongoose.Types.ObjectId(mu));
        });
        req.body.mentionsUsers = mentions;
      }

      // TODO: We need to filter that
      thread.body = req.body.body;
      thread.title = req.body.title;

      thread.save(function(err) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot update the thread'
          });
        } else {
          thread.populate({
            path: 'mentionsUsers',
            select: 'name username'
          }, function(err, doc) {
            res.json(doc);
          });
        }

        Threads.events.publish({
          action: 'updated',
          user: {
            name: req.user.name
          },
          name: thread.title,
          url: config.hostname + '/threads/' + thread._id
        });
      });
    },

    unpublish: function (req, res) {
      var thread = req.thread;

      thread.published = false;
      thread.save(function(err) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot unpublish the thread'
          });
        } else {
          thread.populate({
            path: 'mentionsUsers',
            select: 'name username'
          }, function(err, doc) {
            res.json(doc);
          });
        }

        Threads.events.publish({
          action: 'unpublished',
          user: {
            name: req.user.name
          },
          url: config.hostname + '/threads/' + thread._id,
          name: thread.title
        });
      });
    },

    publish: function (req, res) {
      var thread = req.thread;

      thread.published = true;

      thread.save(function(err) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot publish the thread'
          });
        } else {
          thread.populate({
            path: 'mentionsUsers',
            select: 'name username'
          }, function(err, doc) {
            res.json(doc);
          });
        }

        Threads.events.publish({
          action: 'published',
          user: {
            name: req.user.name
          },
          url: config.hostname + '/threads/' + thread._id,
          name: thread.title
        });
      });
    },

    like: function (req, res) {
      var thread = req.thread;

      var found = false;
      _.each(thread.likes, function (elem) {
        if (elem.toString() == req.user._id.toString()) found = true;
      });

      if (!found){
        console.log ('we add it');

        thread.likes.push (req.user._id);
        thread.youLike = false;

        thread.save(function(err) {
          if (err) {
            return res.status(500).json({
              error: 'Cannot mark the like'
            });
          } else {
            thread.populate({
              path: 'mentionsUsers',
              select: 'name username'
            }, function(err, doc) {
              doc.youLike = true;
              res.json(doc);
            });
          }

          Threads.events.publish({
            action: 'like',
            user: {
              name: req.user.name
            },
            url: config.hostname + '/threads/' + thread._id,
            name: thread.title
          });
        });
      } else {
        thread.populate({
          path: 'mentionsUsers',
          select: 'name username'
        }, function(err, doc) {
          res.json(doc);
        });

      }

    },

    unlike: function (req, res) {
      var thread = req.thread;

      var found = -1 ;
      var count = 0;

      _.each(thread.likes, function (elem) {
        if (elem.toString() == req.user._id.toString()) found = count;
        count ++;
      });

      if (found > -1) {
        console.log ('we remove from it');

        thread.likes.splice (found,1);
        thread.youLike = false;
        thread.markModified('likes');

        thread.save(function(err) {
          if (err) {
            return res.status(500).json({
              error: 'Cannot mark the like'
            });
          } else {
            thread.populate({
              path: 'mentionsUsers',
              select: 'name username'
            }, function(err, doc) {
              thread.youLike = false;
              res.json(doc);
            });
          }

          Threads.events.publish({
            action: 'unlike',
            user: {
              name: req.user.name
            },
            url: config.hostname + '/threads/' + thread._id,
            name: thread.title
          });
        });
      } else {
        thread.populate({
          path: 'mentionsUsers',
          select: 'name username'
        }, function(err, doc) {
          res.json(doc);
        });
      }

    },

    /**
    * Delete a thread
    */
    destroy: function(req, res) {
      var thread = req.thread;

      thread.remove(function(err) {
        if (err) {
          return res.status(500).json({
            error: 'Cannot delete the thread'
          });
        }

        Threads.events.publish({
          action: 'deleted',
          user: {
            name: req.user.name
          },
          name: thread.title
        });

        res.json(thread);
      });
    },

    /**
    * Chech the threads elements list
    */

    fetchByParent: function(req, res) {
      var parentId = req.params.threadId;
      var limit = req.query.limit;
      console.log ('paramid', parentId);

      var query = Thread.find({$or: [{_id: parentId}, {parents: parentId}]
      })
      .sort({
        _id: -1
      })
      .populate('user', 'name username email email')
      .populate('mentionsUsers', 'name username')
      .populate('likes', 'name username email');

      if (limit) {
        query.limit(limit);
      }

      query.exec(function(err, threads) {
        if (err) {
          res.render('error', {
            status: 500
          });
        } else {
          //That must be a little bit time consuming :-( Ofuscate the unpublished comments unless it's yours
          _.each (threads, function (thread) {
            if (thread.published == false && thread.user._id.toString() !== req.user._id.toString() ){
              thread.body = "not published";
            }
            console.log ('youLike', youLike (thread.likes,req.user));
            thread.youLike = youLike (thread.likes,req.user);
          });

          res.json(_.tree(threads, parentId, '_id', 'parent', 'children'));
        }

        Threads.events.publish({
          action: 'listed',
          user: {
            name: req.user.name
          },
          name: req.params.paramId,
          url: config.hostname + '/threads/' + req.thread._id
        });

      });
    },

    allRootThreadsByPopular: function (req,res) {
      var limit = req.query.limit;
      var owner = req.query.owner;
      var searchBy = req.query.searchBy;
      console.log ('hw');
      var search = {'parent': null};

      if (owner) {
        search.user = owner;
      }

      Thread.aggregate(
        [{$match: search },
          {$project: {
            user:1,
            title:1,
            body:1,
            youLike: 1,
            published: 1,
            parents: 1,
            root : 1,
            mentionsUsers: 1,
            changed: 1,
            parent:1,
            likes: 1,
            numblikes: {$size:"$likes"}}
          },
          {$sort: {numblikes: -1 }}]

          , function(err, threads) {
            if (err) {
              return res.status(500).json({
                error: 'Cannot list the threads'
              });
            } else {
              Thread.populate (threads,
                {path: 'user mentionsUsers likes',select: 'name username email'}
                ,function(err, threads) {
                  if (err) {
                    return res.status(500).json({
                      error: 'Cannot list the threads'
                    });
                  } else {

                    _.each (threads, function (thread) {
                      if (thread.published == false && thread.user._id.toString() !== req.user._id.toString() ){
                        thread.body = "not published";
                      }
                      console.log ('youLike',thread.title, thread.likes, youLike (thread.likes,req.user));
                      thread.youLike = youLike (thread.likes,req.user);
                    });

                    res.json (threads);

                    Threads.events.publish({
                      action: 'listed',
                      user: {
                        name: req.user.name
                      },
                      name: 'requested all threads by popular',
                      url: config.hostname + '/threads'
                    });
                  }
                });

              }
            });
          },

          /**
          * List of Threads
          */
          allRootThreads: function(req, res) {



            var limit = req.query.limit;
            var owner = req.query.owner;

            var search = {'parent': null};

            if (owner) {
              search.user = owner;
            }

            console.log ('search', search);

            var query = Thread.find(search).sort('-created').populate('user', 'name username email email')
            .populate('mentionsUsers', 'name username')
            .populate('likes', 'name username email');
            if (limit) {
              query.limit(limit);
            }

            query.exec(function(err, threads) {
              if (err) {
                return res.status(500).json({
                  error: 'Cannot list the threads'
                });
              } else {
                _.each (threads, function (thread) {
                  if (thread.published == false && thread.user._id.toString() !== req.user._id.toString() ){
                    thread.body = "not published";
                  }
                  console.log ('youLike',thread.title, thread.likes, youLike (thread.likes,req.user));
                  thread.youLike = youLike (thread.likes,req.user);
                });

                res.json (threads);
              }




              Threads.events.publish({
                action: 'listed',
                user: {
                  name: req.user.name
                },
                name: 'requested all threads',
                url: config.hostname + '/threads'
              });
            });

          }
        };
      }
