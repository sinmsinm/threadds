exports.models = {

  Thread: {
    id: 'Thread',
    required: ['content', 'title'],
    properties: {
   
      title: {
        type: 'string',
        description: 'Title of the thread'
      },
      content: {
        type: 'string',
        description: 'content of the thread'
      },
      permissions: {
        type: 'Array',
        description: 'Permissions for viewing the thread'
      }
    }
  }
};
