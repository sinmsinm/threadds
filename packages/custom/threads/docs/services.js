'use strict';

exports.load = function(swagger, parms) {

  var searchParms = parms.searchableOptions;

  var list = {
    'spec': {
      description: 'Thread operations',
      path: '/threads',
      method: 'GET',
      summary: 'Get all Threads',
      notes: '',
      type: 'Thread',
      nickname: 'getThreads',
      produces: ['application/json'],
      params: searchParms
    }
  };

  var create = {
    'spec': {
      description: 'Device operations',
      path: '/threads',
      method: 'POST',
      summary: 'Create thread',
      notes: '',
      type: 'Thread',
      nickname: 'createThread',
      produces: ['application/json'],
      parameters: [{
        name: 'body',
        description: 'Thread to create.  User will be inferred by the authenticated user.',
        required: true,
        type: 'Thread',
        paramType: 'body',
        allowMultiple: false
      }]
    }
  };

  swagger.addGet(list)
    .addPost(create);

};
