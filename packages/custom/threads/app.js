'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Threads = new Module('threads');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Threads.register(function(app, auth, database, circles) {

  //We enable routing. By default the Package Object is passed to the routes
  Threads.routes(app, auth, database, circles);

/*  Threads.aggregateAsset('css', 'threads.css');
*/

  //We are adding a link to the main menu for all authenticated users
  Threads.menus.add({
    'roles': ['authenticated'],
    'title': 'My Threads',
    'link': 'all my threads'
  });

  Threads.menus.add({
    'roles': ['authenticated'],
    'title': 'Recents Threads',
    'link': 'all recent threads'
  });

  Threads.menus.add({
    'roles': ['authenticated'],
    'title': 'Popular Threads',
    'link': 'all popular threads'
  });


  Threads.events.defaultData({
    type: 'post',
    subtype: 'thread'
  });


  /*
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Threads.settings({'someSetting':'some value'},function (err, settings) {
      //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Threads.settings({'anotherSettings':'some value'});

    // Get settings. Retrieves latest saved settings
    Threads.settings(function (err, settings) {
      //you now have the settings object
    });
    */

  // Only use swagger.add if /docs and the corresponding files exists
/*  swagger.add(__dirname);
*/
  return Threads;
});
