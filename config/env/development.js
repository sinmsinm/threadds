'use strict'

module.exports = {
  db: 'mongodb://' + (process.env.DB_PORT_27017_TCP_ADDR || 'localhost') + '/threaDDS-devel',
  debug: true,
  logging: {
    format: 'tiny'
  },
  //  aggregate: 'whatever that is not false, because boolean false value turns aggregation off', //false
  aggregate: false,
  mongoose: {
    debug: false
  },
  hostname: 'http://localhost:3000',
  app: {
    name: 'threaDDS - Your threaded discussion tool'
  },
  strategies: {
    local: {
      enabled: true
    },
    landingPage: '/',
    facebook: {
      clientID: 'DEFAULT_APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/facebook/callback',
      enabled: false,
      name: "Facebook"
    },
    twitter: {
      clientID: 'DEFAULT_CONSUMER_KEY',
      clientSecret: 'CONSUMER_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/twitter/callback',
      enabled: false,
      name: "Twitter"
    },
    github: {
      clientID: '66b45b1496e43203fd4c',
      clientSecret: '530697fbf0e414411be5ce1c3d9dc190d223b39c',
      callbackURL: 'http://localhost:3000/api/auth/github/callback',
      enabled: true,
      name: "Github"
    },
    google: {
      clientID: '928625490993-mf044qbg6fkr9nk250cegt7fktc6ork2.apps.googleusercontent.com',
      clientSecret: 'hISN7qZcDkvQUf9NNkS5OzN_',
      callbackURL: 'http://localhost:3000/api/auth/google/callback',
      enabled: true,
      name: "Google +"

    },
    linkedin: {
      clientID: 'DEFAULT_API_KEY',
      clientSecret: 'SECRET_KEY',
      callbackURL: 'http://localhost:3000/api/auth/linkedin/callback',
      enabled: false
    }
  },
  emailFrom: 'SENDER EMAIL ADDRESS', // sender address like ABC <abc@example.com>
  mailer: {
    service: 'SERVICE_PROVIDER', // Gmail, SMTP
    auth: {
      user: 'EMAIL_ID',
      pass: 'PASSWORD'
    }
  },
  secret: 'SOME_TOKEN_SECRET'
}
